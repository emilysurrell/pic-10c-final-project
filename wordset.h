#ifndef WORDSET_H
#define WORDSET_H

#include <QFile>
#include <QTextStream>
#include <iostream>
#include <set>

class WordSet
{
public:
    WordSet();
    WordSet(std::string letters);
    bool enterWord(std::string playerword); //if playerword is valid, remove playerword from set and return true
    int size();
    bool empty();
    std::set<std::string>::iterator begin();
    void erase(std::set<std::string>::iterator it);
private:
    std::set<std::string> validwords;
};

#endif // WORDSET_H
