#include "wordgame.h"
#include "ui_wordgame.h"

WordGame::WordGame(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::WordGame)
{
    ui->setupUi(this);
    welcome();
}

WordGame::~WordGame()
{
    delete ui;
}

void WordGame::welcome()
{
    // initialize widget with welcome page
    QWidget* welcomepage = new QWidget;
    QBoxLayout* layout = new QBoxLayout(QBoxLayout::TopToBottom);
    QLabel* welcome = new QLabel("Welcome to my Word Game!\nClick on the letters to create words.\nA word can be any length.\nYou may use any letter any number of times.\nYou will earn one point for each letter in a correctly guessed word. Make longer words for more points!\nYou have 90 seconds to make as many words as possible.");
    QPushButton* startbutton = new QPushButton("Start Game", this);
    QObject::connect(startbutton, SIGNAL(released()), this, SLOT(startGame())); //move from welcome page to game page
    layout->addWidget(welcome);
    layout->addWidget(startbutton);
    welcomepage->setLayout(layout);
    setCentralWidget(welcomepage);

    //initialize other variables
    score = 0;
    char vowels[5] = {'a', 'e', 'i', 'o','u'};
    char consonants[21] = {'b','c','d','f','g','h','j','k','l','m','n','p','q','r','s','t','v','w','x','y','z'};
    std::srand(std::time(0));
    guessedwords = "";
    letters = "";
    letters += vowels[std::rand() % 5];
    letters += vowels[std::rand() % 5];
    while (letters[1] == letters[0])
        letters[1] = vowels[std::rand() % 5];
    for (int i = 2; i < 8; i++)
    {
        letters += consonants[std::rand() % 21];
        while (true)
        {
            bool keepletter = true;
            for (int j = 2; j < i; j++)
            {
                if (letters[j] == letters[i])
                    keepletter = false;
            }
            if (keepletter)
                break;
            else
                letters[i] = consonants[std::rand() % 21];
        }
    }
}

void WordGame::startGame()
{
    // change widget to game page
    QWidget* gamepage = new QWidget;
    QBoxLayout* layout = new QBoxLayout(QBoxLayout::TopToBottom);

    QLabel* instructions = new QLabel("Click on the letters below to create words!\nWords can be any length.\nYou can use any letter any number of times.");
    messageLabel = new QLabel();
    guessedLabel = new QLabel();
    guessedLabel->setWordWrap(true);
    QLabel* timelabel = new QLabel("Seconds Remaining:");
    secondsDisplay = new QLCDNumber(this);
    secondsDisplay->setMinimumHeight(50);
    seconds = 90;
    secondsDisplay->display(seconds);
    minitimer = new QTimer(this);
    QObject::connect(minitimer,SIGNAL(timeout()), this, SLOT(updateSeconds()));
    layout->addWidget(instructions);
    layout->addWidget(timelabel);
    layout->addWidget(secondsDisplay);
    layout->addWidget(messageLabel);
    layout->addWidget(guessedLabel);

    QGridLayout* letterbuttonslayout = new QGridLayout();

    validwords = WordSet(letters);
    totalwords = validwords.size();

    numguessed = new QLabel("You guessed 0 words.\nYou scored 0 points.") ;
    layout->addWidget(numguessed);

    //map from lowercase letters to uppercase letters

    char lowers[26] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm','n','o','p','q','r','s','t','u','v','w','x','y','z'};
    char uppers[26] = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
    for (int i = 0; i < 26; i++)
        uppercaseMap[lowers[i]] = uppers[i];

    QPushButton* letterbuttons[8];
    const QSize LETTERBUTTONSIZE= QSize(50, 50);
    for (int i = 0; i < 8; i++)
    {
        letterbuttons[i] = new QPushButton(QString(uppercaseMap[letters[i]]), this);
        letterbuttons[i]->setMinimumSize(LETTERBUTTONSIZE);
        QFont font = letterbuttons[i]->font();
        font.setPointSize(10);
        font.setBold(true);
        QPalette pal = letterbuttons[i]->palette();
        pal.setColor(QPalette::Button, QColor(24, 181, 177));
        letterbuttons[i]->setFlat(true);
        letterbuttons[i]->setAutoFillBackground(true);
        letterbuttons[i]->setPalette(pal);
        letterbuttons[i]->update();
        letterbuttons[i]->setFont(font);
    }
    QObject::connect(letterbuttons[0], SIGNAL(released()), this, SLOT(push0()));
    QObject::connect(letterbuttons[1], SIGNAL(released()), this, SLOT(push1()));
    QObject::connect(letterbuttons[2], SIGNAL(released()), this, SLOT(push2()));
    QObject::connect(letterbuttons[3], SIGNAL(released()), this, SLOT(push3()));
    QObject::connect(letterbuttons[4], SIGNAL(released()), this, SLOT(push4()));
    QObject::connect(letterbuttons[5], SIGNAL(released()), this, SLOT(push5()));
    QObject::connect(letterbuttons[6], SIGNAL(released()), this, SLOT(push6()));
    QObject::connect(letterbuttons[7], SIGNAL(released()), this, SLOT(push7()));
    letterbuttonslayout->addWidget(letterbuttons[0], 0, 0);
    letterbuttonslayout->addWidget(letterbuttons[1], 0, 1);
    letterbuttonslayout->addWidget(letterbuttons[2], 0, 2);
    letterbuttonslayout->addWidget(letterbuttons[3], 0, 3);
    letterbuttonslayout->addWidget(letterbuttons[4], 1, 0);
    letterbuttonslayout->addWidget(letterbuttons[5], 1, 1);
    letterbuttonslayout->addWidget(letterbuttons[6], 1, 2);
    letterbuttonslayout->addWidget(letterbuttons[7], 1, 3);

    QBoxLayout* buttonslayout = new QBoxLayout(QBoxLayout::LeftToRight);
    const QSize BUTTONSIZE= QSize(107, 107);
    const int FONTSIZE = 10;

    /*
    QPushButton* giveup = new QPushButton("Give Up", this);
    QObject::connect(giveup, SIGNAL(released()), this, SLOT(giveUp()));
    giveup->setMinimumSize(BUTTONSIZE);
    QFont font = giveup->font();
    font.setPointSize(FONTSIZE);
    font.setBold(true);
    giveup->setFont(font);
    QPalette pal = giveup->palette();
    pal.setColor(QPalette::Button, QColor(240, 89, 24));
    giveup->setAutoFillBackground(true);
    giveup->setFlat(true);
    giveup->setPalette(pal);
    giveup->update();
    buttonslayout->addWidget(giveup);
    */

    buttonslayout->addLayout(letterbuttonslayout);

    QPushButton* backspace = new QPushButton("Backspace", this);
    QObject::connect(backspace, SIGNAL(released()), this, SLOT(backspace()));
    backspace->setMinimumSize(BUTTONSIZE);
    backspace->setMaximumSize(BUTTONSIZE);
    QFont font = backspace->font();
    font.setPointSize(FONTSIZE);
    font.setBold(true);
    backspace->setFont(font);
    QPalette pal = backspace->palette();
    pal.setColor(QPalette::Button, QColor(236, 240, 5));
    backspace->setAutoFillBackground(true);
    backspace->setFlat(true);
    backspace->setPalette(pal);
    backspace->update();
    buttonslayout->addWidget(backspace);

    QPushButton* enter = new QPushButton("Enter Word", this);
    QObject::connect(enter, SIGNAL(released()), this, SLOT(enterWord()));
    enter->setMinimumSize(BUTTONSIZE);
    enter->setMaximumSize(BUTTONSIZE);
    font = enter->font();
    font.setPointSize(FONTSIZE);
    font.setBold(true);
    enter->setFont(font);
    pal = enter->palette();
    pal.setColor(QPalette::Button, QColor(21, 209, 15));
    enter->setAutoFillBackground(true);
    enter->setFlat(true);
    enter->setPalette(pal);
    enter->update();
    buttonslayout->addWidget(enter);

    layout->addLayout(buttonslayout);
    gamepage->setLayout(layout);
    setCentralWidget(gamepage);

    minitimer->start(1000); //1 second
}

void WordGame::push0()
{
    playerword += letters[0];
    messageLabel->setText(QString::fromStdString(uppercase(playerword)));
}
void WordGame::push1()
{
    playerword += letters[1];
    messageLabel->setText(QString::fromStdString(uppercase(playerword)));
}
void WordGame::push2()
{
    playerword += letters[2];
    messageLabel->setText(QString::fromStdString(uppercase(playerword)));
}
void WordGame::push3()
{
    playerword += letters[3];
    messageLabel->setText(QString::fromStdString(uppercase(playerword)));
}
void WordGame::push4()
{
    playerword += letters[4];
    messageLabel->setText(QString::fromStdString(uppercase(playerword)));
}
void WordGame::push5()
{
    playerword += letters[5];
    messageLabel->setText(QString::fromStdString(uppercase(playerword)));
}
void WordGame::push6()
{
    playerword += letters[6];
    messageLabel->setText(QString::fromStdString(uppercase(playerword)));
}

void WordGame::push7()
{
    playerword += letters[7];
    messageLabel->setText(QString::fromStdString(uppercase(playerword)));
}

std::string WordGame::uppercase(std::string playerword)
{
    std::string caps;
    for (unsigned int i = 0; i < playerword.length(); i++)
        caps += uppercaseMap[playerword[i]];
    return caps;
}

void WordGame::backspace()
{
    if (!playerword.empty())
        playerword.pop_back();
    messageLabel->setText(QString::fromStdString(uppercase(playerword)));
}

void WordGame::enterWord()
{
    bool valid = validwords.enterWord(playerword);
    if (valid)
    {
        if (totalwords - validwords.size() != 1)
            guessedwords += ", ";
        guessedwords += uppercase(playerword);
        messageLabel->setText("Correct!");
        guessedLabel->setText(QString::fromStdString("You've guessed: " + guessedwords));
        score += playerword.length();
    }
    else
        messageLabel->setText("Incorrect");
    playerword = "";
    numguessed->setText("You guessed " + QString::number(totalwords - validwords.size()) + " words.\nYou scored " + QString::number(score) + " points.");
    if (totalwords - validwords.size() == 1 && score == 1)
        numguessed->setText("You guessed 1 word.\nYou scored 1 point.");
    else if (totalwords - validwords.size() == 1)
        numguessed->setText("You guessed 1 word.\nYou scored " + QString::number(score) + " points.");
    if (validwords.empty())
        guessedAll();
}
/*
void WordGame::giveUp()
{
    // display and erase all remaining words in validwords set
    std::string words = "You missed the following words:";
    while (!validwords.empty())
      {
         words += "\n" + (uppercase(*validwords.begin()));
         validwords.erase(validwords.begin());
      }

    // end of game display
    QWidget* endpage = new QWidget;
    QBoxLayout* layout = new QBoxLayout(QBoxLayout::TopToBottom);

    layout->addWidget(guessedLabel);

    QLabel* missedwords = new QLabel(QString::fromStdString(words));
    layout->addWidget(missedwords);

    QPushButton* playagain = new QPushButton("Play Again", this);
    QObject::connect(playagain, SIGNAL(released()), this, SLOT(welcome()));
    layout->addWidget(playagain);

    endpage->setLayout(layout);
    setCentralWidget(endpage);
}
*/

void WordGame::guessedAll()
{
    // end of game display
    QWidget* endpage = new QWidget;
    QBoxLayout* layout = new QBoxLayout(QBoxLayout::TopToBottom);

    QLabel* congrats = new QLabel("Congratulations!\nYou guessed all of the words!\nYou earned " + QString::number(score) + " points.");
    layout->addWidget(congrats);

    layout->addWidget(guessedLabel);

    QPushButton* playagain = new QPushButton("Play Again", this);
    QObject::connect(playagain, SIGNAL(released()), this, SLOT(welcome()));
    layout->addWidget(playagain);

    endpage->setLayout(layout);
    setCentralWidget(endpage);
}

void WordGame::timeOut()
{
    minitimer->stop();
    // end of game display
    QWidget* endpage = new QWidget;
    QBoxLayout* layout = new QBoxLayout(QBoxLayout::TopToBottom);

    QLabel* timeout = new QLabel("Time Out!");
    layout->addWidget(timeout);
    layout->addWidget(numguessed);

    layout->addWidget(guessedLabel);

    QPushButton* playagain = new QPushButton("Play Again", this);
    QObject::connect(playagain, SIGNAL(released()), this, SLOT(welcome()));
    layout->addWidget(playagain);

    endpage->setLayout(layout);
    setCentralWidget(endpage);
}

void WordGame::updateSeconds()
{
    secondsDisplay->display(--seconds);
    if (seconds <= 0)
        timeOut();
}
