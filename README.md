# README #

## This repository is for PIC 10C, Final Project at UCLA

Quarter: Spring 2020

Instructor: Ricardo Salazar

Student: Emily Surrell

### Goal: Create Word Finding Game ###

* Determine a list of valid English words
* In each round of play, the program will randomly select 8 unique letters from the English alphabet: 2 vowels and 6 consonants
* The goal of the player is to use those 8 letters to create as many words as possible! Words will be counted as valid if they appear on the word list
* Visual design using Qt

There were quite a few challenges in designing the game. Below are some questions that I asked myself while working on the project and how I addressed them.

### What is a good word list?
For this game, we need a list of correct words in order to check if a player's word is valid. Finding a good list, and the best way to use it, was perhaps one of the most frustrating parts of this project. Originally, I used [a list of 3,000 common English words](https://www.ef.edu/english-resources/english-vocabulary/top-3000-words/). At first glance, this list seemed okay because it included the kind of words that I wanted to include in my game (common standard English words, no proper nouns, slang, abbreviations, etc.)

After coding the game and playing it with this list, I realized that I did not like the list for the following reason: there were too many instances in which I knew a word, but the word was not on the list. I decided that I needed to find a longer and more inclusive word list.
With more searching, I found [a list of 9,894 words](https://github.com/first20hours/google-10000-english/blob/master/google-10000-english-no-swears.txt). But, playing with this list, I encountered the opposite problem: there were many words on the list that I didn't think a player should be expected to guess. This included people's first names, cities, states, countries, abbreviations, and slang. For example, I wouldn't want a player to be stuck trying to uncover the last missing word, only for something like "html" or "aargh" to be the missing word.

I then tried to go through this list and remove these words that I didn't want to include in the game. After these revisions, I saved this list as [commonwords.txt](https://bitbucket.org/emilysurrell/pic-10c-final-project/src/master/commonwords.txt). But, even after editing a list of nearly ten thousand words twice through, I still wasn't happy with the word list.

I realized that I had given myself the following dilemma: a list too short will not give credit to a player who guesses a good word that is not on the list, while a list too long will unreasonably expect a player to guess strange words. Due to the difficulty in trying to create a perfect list that meets both of these criteria, I decided to change my strategy of defining a good list. Instead of trying to guess all possible words, the goal of the game will be to guess as many words as possible within a time limit. With this new game structure, the word list should be as long and inclusive as possible.

So I kept searching. I found [a list of 194,433 words](http://gwicks.net/dictionaries.htm), which I saved as [english3.txt](https://bitbucket.org/emilysurrell/pic-10c-final-project/src/master/english3.txt). Finally, I settled on [a list of 370,104 words](https://github.com/dwyl/english-words/blob/master/words_alpha.txt), saved as [words_alpha.txt](https://bitbucket.org/emilysurrell/pic-10c-final-project/src/master/words_alpha.txt).

### How will the program read and process the word list?
This was easier said than done. I knew from PIC 10B that C++ can read in a text file using [ifstream](http://www.cplusplus.com/reference/fstream/ifstream/), but when I tried to use this in Qt Creator, the program didn't seem to recognize my text file. I reached out to the TA for help, and he explained that I needed to add my text file to a Qt resource file, [resource.qrc](https://bitbucket.org/emilysurrell/pic-10c-final-project/src/master/resource.qrc), and make use of the [QFile](https://doc.qt.io/qt-5/qfile.html) and [QTextStream](https://doc.qt.io/qt-5/qtextstream.html) classes. He referred me to [this StackOverflow post](https://stackoverflow.com/questions/2612103/qt-reading-from-a-text-file/2612679#2612679). With Jason's help, I was able to code the `WordSet::WordSet(std::string letters)` constructor found in [wordset.cpp](https://bitbucket.org/emilysurrell/pic-10c-final-project/src/master/wordset.cpp).

### How will a player's word be verified by the word list?
Storage and use of the word list occurs in the following sequence of events:

1. In `void WordGame::welcome()` in [wordgame.cpp](https://bitbucket.org/emilysurrell/pic-10c-final-project/src/master/wordgame.cpp), 8 random letters are selected and stored in `std::string letters`.
1. In `void WordGame::startGame()`, `WordSet validwords = WordSet(letters)`. This calls the constructor of my `WordSet` class, which contains a [std::set<std::string>](http://www.cplusplus.com/reference/set/set/).
1. The `WordSet` constructor opens [words_alpha.txt](https://bitbucket.org/emilysurrell/pic-10c-final-project/src/master/words_alpha.txt) as a [QFile](https://doc.qt.io/qt-5/qfile.html) and reads it line by line (word by word, since each word is on its own line). It determines if each word is valid based on the 8 selected letters, and adds the word to `std::set<std:string> validwords` if the word is valid.
1. When a player enters a word `std::string playerword`, `void WordGame::enterWord()` calls `bool WordSet::enterWord(std::string playerword)`. If playerword exists in the set, then the word is valid and it is removed from the set. That way, it will not be valid if the player guesses the same word a second time.


### What buttons will be available for the player?
Originally, I had the following buttons in mind: 7 letter buttons (2 vowels, 5 consonants), a "Give Up" button, and an "Enter Word" button. The player can press each letter button to type in a letter, press the "Enter Word" button to submit a word, and press the "Give Up" button to quit the game. After playing with this version, I decided to add a "Backspace" button that would allow the player to erase the last pressed letter. After deciding that I would make my word list as inclusive as possible and implement a timer, I decided to remove the "Give Up" button because a player could alternatively just wait for the timer to run out. Lastly, for a more balanced visual appeal, I decided to have 8 letter buttons (2 vowels, 6 consonants) instead of 7 letter buttons.

Take a look at the following slots found in [wordgame.cpp](https://bitbucket.org/emilysurrell/pic-10c-final-project/src/master/wordgame.cpp), which correspond to the [QPushButtons](https://doc.qt.io/qt-5/qpushbutton.html) available to the player:

`void WordGame::push0()`

`void WordGame::push1()`

`void WordGame::push2()`

`void WordGame::push3()`

`void WordGame::push4()`

`void WordGame::push5()`

`void WordGame::push6()`

`void WordGame::push7()`

`void WordGame::backspace()`

`void WordGame::enterWord()`

`//void WordGame::giveUp()` <- commented out code from earlier version of game, newer version has a timer instead

### What colors will the buttons be?
In general terms, I wanted the "Backspace" button to be yellow, the "Enter Word" button to be green, the "Give Up" button to be red (before I replaced this button with a timer) and each letter button to be something like blue or gray. I learned about changing the color of a [QPushButton](https://doc.qt.io/qt-5/qpushbutton.html) by reading [this StackOverflow post](https://stackoverflow.com/questions/21685414/qt5-setting-background-color-to-qpushbutton-and-qcheckbox#comment35750181_21685414), learning about [QColor](https://doc.qt.io/archives/qt-4.8/qcolor.html), and playing around with [RGB numbers](https://www.google.com/search?q=rgb+color+picker&rlz=1C1CHBF_enUS746US746&oq=rgb+&aqs=chrome.1.69i57j0l7.4947j0j7&sourceid=chrome&ie=UTF-8) to find just the right colors.
`QPushButton::setFlat(true)` was needed to ensure that the chosen color applies to the entire button, not just the outline of the button.

### How will the buttons be arranged?
I played around with a few different ideas, but ultimately decided that it would be most visually appealing to have 8 letter buttons in a 2 row by 4 column grid, with the "Backspace" button to the right of the letter buttons and the "Enter Word" button to the right of the "Backspace" button. I achieved this by using [QGridLayout](https://doc.qt.io/archives/qt-4.8/qgridlayout.html) and [QBoxLayout](https://doc.qt.io/qt-5/qboxlayout.html).

I also wanted to have some control over the size of each button, so I referred to [this StackOverflow post](https://stackoverflow.com/questions/36063103/how-to-set-precise-size-of-qpushbutton) for guidance.

### Will there be a timer?
At first, I did not want to include a timer because I preferred a more relaxed version of the game in which a player had unlimited time to try to guess all possible words. However, it was difficult to put together an appropriate word list for this version. So, I researched [QTimer](https://doc.qt.io/qt-5/qtimer.html) and decided to allow the player 90 seconds to guess as many words as they can.

Implementing the timer was a bit tricky. I referred to [this StackOverflow post](https://stackoverflow.com/questions/27341944/substitute-for-sleep-function-in-qt-c) and [this Youtube video](https://www.youtube.com/watch?v=HkmQtp0QXco) to figure out how to get it working. At first, I thought that I would simply be creating a 90-second timer. But, as it turns out, what worked was to make a one-second timer, `QTimer* minitimer`, that runs 90 times. This was important for displaying the timer.

### How will I display the timer?
I displayed the timer as a [QLCDNumber](https://doc.qt.io/qt-5/qlcdnumber.html) called `secondsDisplay`, which can be found in the `void WordGame::startGame()` slot in [wordgame.cpp](https://bitbucket.org/emilysurrell/pic-10c-final-project/src/master/wordgame.cpp). With the help of [this list of QLCDNumber member functions](https://doc.qt.io/qt-5/qlcdnumber-members.html), I set up `secondsDisplay` to display the value of `int seconds` and updates every second with the slot `void WordGame::updateSeconds()`. `seconds` is initialized to 90, and counts down to 0. When it reaches 0 the slot `void WordGame::timeOut()` is called, in which `minitimer->stop()` is called so that the one-second timer `minitimer` no longer repeats.

### How will I assign points?
I figured that longer words should earn more points than shorter words. As a simple solution, I made each letter of a correctly guessed word worth one point. Thus, a correct word `std::string playerword` earns `playerword.length()` points. See the update of `int score` in `void WordGame::enterWord()` in [wordgame.cpp](https://bitbucket.org/emilysurrell/pic-10c-final-project/src/master/wordgame.cpp) (line 261).

### How do I choose the letters randomly?
Letters are randomly chosen in the `void WordGame::welcome()` slot in [wordgame.cpp](https://bitbucket.org/emilysurrell/pic-10c-final-project/src/master/wordgame.cpp). You will see in [wordgame.h](https://bitbucket.org/emilysurrell/pic-10c-final-project/src/master/wordgame.h) that I added `#include <cstdlib>` and `#include <ctime>` so that I can use `std::srand(std::time(0))` and then `std::rand()` to produce random integers. These integers, after application of the modulo operator `%` to specify a range, were then used as indexes to select letters from the alphabet (manually stored in `char vowels[5]` and `char consonants[21]`) and add each selected letter to `std::string letters`. Thus, `letters` is a `std::string` that becomes 8 letters in length. My code randomly chooses 2 *unique* vowels and 6 *unique* consonants to go in `letters`. To review how to code random integers in C++, I referred to [this guide](https://www.bitdegree.org/learn/random-number-generator-cpp).

### Can the player play again? Will they get different letters each game?
Yes, I set it up so that the player can press a "Play Again" button at the end of the game and a new game will be generated. This is the purpose of the `void WordGame::welcome()` slot, in which the 8 random letters are selected and a welcome page with a "Start Game" button becomes visible. Rather than putting this section of code in the `WordGame` constructor, I made a `welcome()` slot so that it can be called multiple times to successively generate new games. It is first called in the `WordGame` constructor, and also called each time the "Play Again" button is pressed.

### How do I want the text to appear?
##### All Caps
To make gameplay easier on the eyes, I decided that the letter buttons and the display of guessed words should be seen in all caps. However, I didn't want to alter my already-working code which read in [a text file of lowercase English words](https://bitbucket.org/emilysurrell/pic-10c-final-project/src/master/words_alpha.txt) and was able to verify lowercase words submitted by the player. Thus, I thought it would be easiest to take a surface-level approach, and simply alter the display of these letters and words without making any changes to the internal storage of data and verification process. I did this by creating a `std::map<char, char>` which maps the lowercase alphabet to the uppercase alphabet (see lines 91-96 in [wordgame.cpp](https://bitbucket.org/emilysurrell/pic-10c-final-project/src/master/wordgame.cpp) under `void WordGame::startGame()`). I referred to [this StackOverflow post](https://stackoverflow.com/questions/3709031/mapstring-string-how-to-insert-data-in-this-map) to review maps in C++. My map converts lowercase characters to uppercase characters, and it is used in `std::string WordGame::uppercase(std::string playerword)` to convert lowercase words to uppercase words.

##### Font Size
I wanted the text on the buttons to be large and easy to read, so I referenced [this post](https://forum.qt.io/topic/81986/how-to-set-the-font-size-of-my-qpushbutton) to learn how to make the font size bigger.

##### Bold
I also referenced [this forum post](https://www.qtcentre.org/threads/15175-How-to-change-font-size-in-QLabel(QLabel-created-from-Qdesigner)) to learn how to make the text bold on the buttons.

##### Text Wrapping
At first, I displayed the guessed words with each word on its own line, which was easy to read. However, as this list would grow, the game window would get taller and this would become inconvenient for the player as the number of guessed words increased. So, I figured that it would be better to display the words in one line, separated by commas. This created the opposite problem: as the list would grow, the game window would get wider. To resolve this, I made the text of the guessed words list [wrap to the next line](https://wiki.qt.io/Word_Wrap_of_Text_in_QLabel).

### What are some possible future improvements for the game?
* Create a leaderboard: before the start of each game, a player types in their name or initials. Their score at the end of the game would be saved with their name, and at the end of each game the leaderboard would be sorted from high to low scores and the top ten scores would be displayed.
* Create different game modes based on number of letters to choose from: default is 8 letters, create a 6 letter version and a 10 letter version

## Screenshot

![image](https://bitbucket.org/emilysurrell/pic-10c-final-project/downloads/FinalProjectImage.PNG)