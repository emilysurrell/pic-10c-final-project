#include "wordset.h"

WordSet::WordSet() {} //should not be used

WordSet::WordSet(std::string letters)
{
    //words from text file that can be formed using only those 8 letters are valid

    QFile file(":/words_alpha.txt"); //accessed via resource.qrc
    if(!file.open(QIODevice::ReadOnly))
    {
        std::cout << "file failed to open" << std::endl;
        throw std::runtime_error("file failed to open");
    }
    else
    {
        QTextStream in(&file);
        while(!in.atEnd())
        {
            QString qword = in.readLine();
            std::string word = qword.toStdString();
            bool wordvalid = true;
            for (unsigned int i = 0; i < word.length(); i++)
            {
                bool wordivalid = false;
                for (int j = 0; j < 8; j++)
                {
                    if (word[i] == letters[j])
                    {
                        wordivalid = true; //word[i] is a valid letter
                    }
                }
                if (wordivalid==false)
                {
                    wordvalid = false;
                }
            }

            if (wordvalid)
            {
                validwords.insert(word);
            }
        }
        file.close();
    }
}

bool WordSet::enterWord(std::string playerword) //if playerword is valid, remove playerword from set and return true
{
    bool valid = validwords.count(playerword);
    validwords.erase(playerword);
    return valid;
}

int WordSet::size()
{
    return validwords.size();
}

bool WordSet::empty()
{
    return validwords.empty();
}

std::set<std::string>::iterator WordSet::begin()
{
    return validwords.begin();
}

void WordSet::erase(std::set<std::string>::iterator it)
{
    validwords.erase(it);
}
