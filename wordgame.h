#ifndef WORDGAME_H
#define WORDGAME_H

#include "wordset.h"
#include <cstdlib>
#include <ctime>
#include <QMainWindow>
#include <QWidget>
#include <QBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QTimer>
#include <QLCDNumber>

QT_BEGIN_NAMESPACE
namespace Ui { class WordGame; }
QT_END_NAMESPACE

class WordGame : public QMainWindow
{
    Q_OBJECT

public:
    WordGame(QWidget *parent = nullptr);
    ~WordGame();

public slots:
    void startGame();
    void push0();
    void push1();
    void push2();
    void push3();
    void push4();
    void push5();
    void push6();
    void push7();
    void backspace();
    void enterWord();
    //void giveUp();
    void guessedAll();
    void welcome();
    void timeOut();
    void updateSeconds();

private:
    std::string uppercase(std::string playerword);
    QLabel* messageLabel;
    QLabel* guessedLabel;
    QLabel* numguessed;
    std::string letters;
    std::string playerword;
    std::string guessedwords;
    std::map<char,char> uppercaseMap;
    WordSet validwords;
    int totalwords;
    int score;
    QLCDNumber* secondsDisplay;
    QTimer* minitimer;
    int seconds;
    Ui::WordGame *ui;

};
#endif // WORDGAME_H
